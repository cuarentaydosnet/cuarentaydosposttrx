const argon2 = require('argon2');
const requestJson = require('request-json');
const uuid = require('uuid/v4');

exports.handler = function(event,context,callback){
    console.log("Create User")
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    const mitoken = event.stageVariables.tokenA1;
    const api = event.stageVariables.api
    console.log(event)
    
    
    var httpClient = requestJson.createClient(db);
    if(event.body || JSON.parse(event.body).tipo){
        var cuerpo = JSON.parse(event.body)
        var tipo = cuerpo.tipo
        switch(tipo){
            case 0 : 
                console.log("TRX: ingreso en efectivo");
                var newTrx = {
                    "_id" : uuid(),
                    "fecha" : Date.now(),
                    "tipo" : 0,
                    "abono" : event.body.abono,
                    "importe" : event.body.importe
                };
                break;
            case 1 : 
                console.log("TRX: retirada de efectivo");
                var newTrx = {
                    "_id" : uuid(),
                    "fecha" : Date.now(),
                    "tipo" : 1,
                    "cargo" : event.body.cargo,
                    "importe" : event.body.importe
                };
                break;
            case 2 : 
                console.log("TRX: traspaso");
                var cliente = requestJson.createClient(api);
                cliente.headers['Authorization'] = mitoken;
                cliente.headers['Content-Type'] = 'application/json';
                //Comprobamos que la cuenta de origen es del usuario (con el token)
                var consulta ="cuentas/" + cuerpo.cuentaCargo + "?showid=true"
                cliente.get(consulta, function(err,res,cuenta){            
                    var owner = cuenta[0].owner;
                    if(owner != user._id){
                        console.log("el usuario no coincide")
                        var response = {"statusCode": 403,"headers" : {"Access-Control-Allow-Origin": "*"}, "body": "{\"msg\" : \"El usuario no coincide\"}","isBase64Encoded": false};
                        return callback(null,response);
                    }else{
                        console.log("el usuario es correcto")
                        //comprobamos que la cuenta origen tiene saldo    
                        var consulta ="cuentas/" + cuerpo.cuentaCargo + "/saldo"
                        cliente.get(consulta, function(err,res,cuenta){
                            var saldo = cuenta.saldo;
                            if(saldo < cuerpo.importe){
                                console.log("No hay saldo suficiente")
                                var response = {"statusCode": 403,"headers" : {"Access-Control-Allow-Origin": "*"}, "body": "{\"msg\" : \"No hay saldo sugiciente\"}","isBase64Encoded": false};
                                return callback(null,response);
                            }else{
                                console.log("la cuenta tiene saldo")
                                //hacemos el cargo en la cuenta origen
                                var consulta ="cuentas/" + cuerpo.cuentaCargo + "/movimientos"
                                var payload = { "concepto" :  cuerpo.concepto ,"importe" :  cuerpo.importe , "tipo" : 0 };
                                cliente.post(consulta, payload, function(err,res,movimiento){
                                    if(err){
                                        console.log("no se ha podido añadir el movieminto")
                                        var response = {"statusCode": 502,"headers" : {"Access-Control-Allow-Origin": "*"}, "body": {"msg" : "Ha fallado la transaccion"},"isBase64Encoded": false};
                                        return callback(null,response);
                                    }else{
                                        var movimientoCargo = movimiento.id
                                        //hacemos el abono en la cuenta destino
                                        var consulta ="cuentas/" + cuerpo.cuentaAbono + "/movimientos"
                                        var payload = { "concepto" :  cuerpo.concepto ,"importe" :  cuerpo.importe , "tipo" : 1 };
                                        cliente.post(consulta, payload, function(err,res,movimiento){
                                            if(err){
                                                console.log("el abono ha fallado. Hay que deshacer la transaccion")
                                                var response = {"statusCode": 502,"headers" : {"Access-Control-Allow-Origin": "*"}, "body": "{\"msg\" : \"Ha fallado la transaccion\"}","isBase64Encoded": false};
                                                return callback(null,response);
                                            }else{
                                                var movimientoAbono = movimiento.id
                                                //grabamos la transaccion
                                                var newTrx = {
                                                    "_id" : uuid(),
                                                    "fecha": Date.now(),
                                                    "cuentaCargo" : cuerpo.cuentaCargo,
                                                    "cuentaAbono" : cuerpo.cuentaAbono,
                                                    "movimientoCargo" : movimientoCargo,
                                                    "movimientoAbono" : movimientoAbono
                                                }
                                                var consulta ="trx?" + apikey 
                                                var mlabClient = requestJson.createClient(db);
                                                mlabClient.post(consulta, newTrx, function(err,res,body){
                                                    if(err){
                                                        console.log("el abono ha fallado. Hay que deshacer la transaccion")
                                                        var response = {"statusCode": 502,"headers" : {"Access-Control-Allow-Origin": "*"}, "body": {"msg" : "Ha fallado la transaccion"},"isBase64Encoded": false};
                                                        return callback(null,response);
                                                    }else{
                                                        console.log("La transaccion ha terminado OK" + JSON.stringify(newTrx))
                                                        var response = {"statusCode": 200,"headers" : {"Access-Control-Allow-Origin": "*"},"body": JSON.stringify(body),"isBase64Encoded": false};
                                                        return callback(null,response); 
                                                    }
                                                })
                                                
                                            }
                                            
                                        })
                                    }
                                    
                                })
                            }
                            
                        })
                    }
                })
                break;
        }
    }
};
                                
                
                
                
                
                
                
                